import React from "react";
import { resizeImage } from "../../utils/imageResize";

const ImageCaptureButtion = (props) => {

    const { uploadImage, disabled } = props;

    const handleImageChange = event => {
        if (event.target.files && event.target.files[0]) {
            resizeImage(event.target.files[0])(uploadImage);
        }
    }

    return (
        <>
            <div className="image-capture-button-container">
                <label className="image-capture-label">
                    <span className="image-capture-icon"
                        className={`image-capture-icon ${disabled ? "disabled" : ""}`}>&#128247;</span>
                    <input
                        className="image-capture-input"
                        disabled={disabled}
                        type="file"
                        accept="image/*"
                        capture
                        name="image"
                        onChange={handleImageChange}
                    />
                </label>

            </div>

            <style jsx>{`
            .image-capture-button-container {
                position: absolute;
                bottom: 12px;
            }

            .image-capture-label {
                display: flex;
                align-items: center;
                width: 70px;
                height: 70px;
                border: 1px black solid;
                border-radius: 50%;
                background-color: lightgray;
            }

            .image-capture-label:hover {
                cursor: pointer;
            }

            .image-capture-icon {
                padding: 17px;
                font-size: 35px;
            }

            .image-capture-icon.disabled {
                animation-name: spin;
                animation-duration: 3000ms;
                animation-iteration-count: infinite;
                animation-timing-function: linear; 
            }

            input.image-capture-input {
                display: none;
            }

            @keyframes spin {
                from {
                    transform:rotate(0deg);
                }
                to {
                    transform:rotate(360deg);
                }
            }
            `}
            </style>
        </>

    )
}

export default ImageCaptureButtion;