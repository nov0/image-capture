import React from "react";
import ImageDeleteButton from "./ImageDeleteButton";

const ImagePreview = ({ images, imageIndex, closeImagePreview, changeImage, deleteImage }) => {
    const showPreviousButton = imageIndex > 0;
    const showNextButton = imageIndex < images.length - 1;
    return (
        <>
            <div className="image-preview-container">
                <img className="image-preview-content" src={images[imageIndex].imageUrl} />
                <button
                    className="image-preview-button-close"
                    type="button"
                    onClick={closeImagePreview}>
                    &#x274C;
                    </button>
                <div className="image-preview-controls-container">
                    <div className="image-preview-controls">
                        <div className="image-preview-nav">
                            {showPreviousButton &&
                                <button
                                    className="image-preview-button image-preview-button-previous"
                                    type="button"
                                    onClick={() => changeImage(-1)}
                                >
                                    &#10094;
                                    </button>
                            }
                        </div>
                        <div>
                            <span className="image-preview-info">{`${imageIndex + 1}/${images.length}`}</span>
                            <ImageDeleteButton deleteImage={deleteImage} imageIndex={imageIndex} />
                        </div>
                        <div className="image-preview-nav">
                            {showNextButton &&
                                <button
                                    className="image-preview-button image-preview-button-next"
                                    type="button"
                                    onClick={() => changeImage(1)}
                                >
                                    &#10095;
                                    </button>
                            }
                        </div>
                    </div>
                </div>

            </div>
            <style jsx>{`
                .image-preview-container {
                    padding: 0;
                    background: white;
                    position:absolute;
                    top:0px;
                    right:0px;
                    bottom:0px;
                    left:0px;
                    max-width: 768px;
                    margin: 0 auto;
                }
                
                .image-preview-content {
                    position: absolute;
                    top: 0;
                    bottom: 0;
                    margin: auto;
                    width: 100%;
                    height: auto;
                    max-height: 100%;
                    max-width: 100%;
                }

                .image-preview-button-close {
                    position:absolute;
                    top: 10px;
                    right: 10px;
                    font-size: 25px;
                    border: none;
                    background: transparent;
                }

                .image-preview-controls-container {
                    position: relative;
                    top: 90%;
                    height: 0;
                }

                .image-preview-controls {
                    display: flex;
                    justify-content: space-between;
                    align-items: center;
                    align-content: center;
                    padding: 10px;
                }

                .image-preview-nav {
                    min-width: 85px;
                }

                .image-preview-button {
                    font-size: 40px;
                    width: 55px;
                    height: 55px;
                    border: none;
                    text-align: center;
                    background: rgba(217,217,217, 0.6);
                }

                .image-preview-button-next {
                    right: 10px;
                }

                .image-preview-button-previous {
                    left: 10px;
                }

                .image-preview-button:hover,
                .image-preview-button-close:hover {
                    cursor: pointer;
                }

                .image-preview-info {
                    font-size: 20px;
                    padding: 7px 20px;
                    border-radius: 6px;
                    color: black;
                    background-color: white;
                    margin-right: 20px
                }
            `}</style>
        </>
    )

}

export default ImagePreview;
