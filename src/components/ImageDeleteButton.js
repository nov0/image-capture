import React from "react";

const ImageDeleteButton = ({ className, imageIndex, deleteImage}) => (
    <>
        <span
            className={`image-delete-button ${className}`}
            onClick={() => deleteImage(imageIndex)}
        >&#128169;</span>
        <style jsx>{`
            .image-delete-button {
                background-color: antiquewhite;
                border-radius: 6px;
                text-align: center;
                font-size: 20px;
                padding: 7px;
            }

            .image-delete-button:hover {
                cursor: pointer;
            }

            .image-delete-button-gallery {
                position: absolute;
                bottom: 20px;
                right: 20px;
              }
            `}
        </style>
    </>
)

export default ImageDeleteButton;