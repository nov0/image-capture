import React, { Component } from "react";
import { uploadImage } from "../../utils/imageBB";

class ImageUpload extends Component {

    async componentDidMount() {
        const response = await uploadImage(this.props.imageForUpload);
        this.props.addImageToGallery(response);
    }
 
    render() {
        return (
            <>
                <div className="image">
                    <div className="image-upload-overlay"></div>
                    <img
                        height="200"
                        width="200"
                        src={this.props.imageForUpload}
                    />
                </div>
                <style jsx>{`
                    .image-upload-overlay {
                        position: fixed;
                        width: 200px;
                        height: 200px;
                        background-color: rgba(255,255,255,0.5);
                    }
                `}</style>
            </>
        )
    }
}

export default ImageUpload;