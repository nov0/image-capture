
const IMAGE_API_URL = "https://api.imgbb.com/1/upload"
const EXPIRATION_IN_SECONDS = "86400"
const API_KEY = "4c334c64d86558de30ea5e422a6c0283"

export const uploadImage = async (image) => {
    let formData = new FormData();
    formData.append('image', image.split(",")[1]);

    const response = await fetch(`${IMAGE_API_URL}?expiration=${EXPIRATION_IN_SECONDS}&key=${API_KEY}`, {
        method: "POST",
        body: formData
    })

    const { data } = await response.json();
    console.log("Image response data: ", data);
    return {
        imageId: data.id,
        imageUrl: data.url,
        thumbnail: data.thumb.url
    };
}
