import Resizer from 'react-image-file-resizer';

const MAX_WIDTH = 1200;
const MAX_HEIGH = 1600

export const resizeImage = image => imgeResizeHandler => {
    Resizer.imageFileResizer(
        image,
        MAX_WIDTH,
        MAX_HEIGH,
        'JPEG',
        99, // not working well this quality option
            // big differences is between 100% (image size ~ 3.2MB) and 99% (image size ~ 0.7MB)
        0,
        imgeResizeHandler,
        'base64'
    );
}
