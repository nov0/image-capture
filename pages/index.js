import Head from 'next/head'
import ImageCaptureButtion from '../src/components/ImageCaptureButton'
import { Component } from 'react';
import ImagePreview from '../src/components/ImagePreview';
import ImageDeleteButton from '../src/components/ImageDeleteButton';
import ImageUpload from '../src/components/ImageUpload';

class Home extends Component {

  state = {
    images: [],
    selectedImageIndex: 0,
    imagePreviewOpened: false,
    imageUploading: false,
    imageForUpload: undefined
  }

  addImageToGallery = image => {
    this.setState({
      ...this.state,
      images: [...this.state.images, image],
      imageUploading: false,
      imageForUpload: undefined
    })
  }

  handleImageClick = imageIndex => {
    this.setState({
      ...this.state,
      selectedImageIndex: imageIndex,
      imagePreviewOpened: true
    })
  }

  closeImagePreview = () => {
    this.setState({
      ...this.state,
      selectedImageIndex: 0,
      imagePreviewOpened: false
    })
  }

  changeImage = offset => {
    console.log("change imate: ", offset);
    this.setState({
      selectedImageIndex: this.state.selectedImageIndex + offset
    })
  }

  uploadImage = imageForUpload => {
    this.setState({
      ...this.state,
      imageForUpload,
      imageUploading: true
    })
  }

  deleteImage = imageIndex => {
    const imagesArray = [...this.state.images];
    imagesArray.splice(imageIndex, 1);
    this.setState({
      ...this.state,
      images: [...imagesArray],
      selectedImageIndex:
        this.state.selectedImageIndex > 0
          ? imageIndex - 1
          : 0,
      imagePreviewOpened: this.state.imagePreviewOpened && imagesArray.length - 1 >= 0
    })
  }

  render() {
    const { images, imagePreviewOpened, selectedImageIndex, imageUploading, imageForUpload } = this.state;

    return (
      <div className="container">
        <Head>
          <title>Image capturing</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <main>
          <h1 className="title">
            Welcome to image capture
          </h1>

          <div className="image-galery">
            {
              images.map((image, $index) => (
                <div className="image pointer" key={$index}>
                  <img
                    width="200"
                    height="200"
                    src={image.thumbnail}
                    onClick={() => this.handleImageClick($index)}
                  />
                  <ImageDeleteButton
                    className="image-delete-button-gallery"
                    imageIndex={$index}
                    deleteImage={this.deleteImage} />
                </div>
              ))
            }
            {imageUploading && (
              <ImageUpload
                imageForUpload={imageForUpload}
                addImageToGallery={this.addImageToGallery}/>
              )
            }
          </div>
        </main>

        <ImageCaptureButtion uploadImage={this.uploadImage} disabled={imageUploading} />

        {imagePreviewOpened && images.length > 0 &&
          <ImagePreview
            images={images}
            imageIndex={selectedImageIndex}
            changeImage={this.changeImage}
            deleteImage={this.deleteImage}
            closeImagePreview={this.closeImagePreview} />
        }

        <style jsx>{`
        .container {
          padding: 0;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
          margin: 0 auto;
          max-width: 768px;
          background-color: white;
          height: 100vh;
        }

        main {
          padding: 10px 0;
          flex: 1;
          display: flex;
          flex-direction: column;
          align-items: center;
        }

        a {
          color: inherit;
          text-decoration: none;
        }

        .title a {
          color: #0070f3;
          text-decoration: none;
        }

        .title a:hover,
        .title a:focus,
        .title a:active {
          text-decoration: underline;
        }

        .title {
          margin: 0;
        }

        .title,
        .description {
          text-align: center;
        }

        .description {
          line-height: 1.5;
          font-size: 1.5rem;
        }

        code {
          background: #fafafa;
          border-radius: 5px;
          padding: 0.75rem;
          font-size: 1.1rem;
          font-family: Menlo, Monaco, Lucida Console, Liberation Mono,
            DejaVu Sans Mono, Bitstream Vera Sans Mono, Courier New, monospace;
        }

        .image-galery {
          display: flex;
          align-items: center;
          justify-content: center;
          flex-wrap: wrap;
          max-width: 800px;
          margin-top: 3rem;
        }

        .image {
          margin: 1rem;
          position: relative;
          padding: 10px;
          color: inherit;
          text-decoration: none;
          border: 1px solid #eaeaea;
          border-radius: 10px;
          transition: color 0.15s ease, border-color 0.15s ease;
        }

        .image:hover,
        .image:focus,
        .image:active {
          color: #0070f3;
          border-color: #0070f3;
          cursor: pointer;
        }

        .image h3 {
          margin: 0 0 1rem 0;
          font-size: 1.5rem;
        }

        .image p {
          margin: 0;
          font-size: 1.25rem;
          line-height: 1.5;
        }

        .logo {
          height: 1em;
        }

        @media (max-width: 600px) {
          .grid {
            width: 100%;
            flex-direction: column;
          }
        }
      `}</style>

        <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          background-color: #e6e6e6;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }
        * {
          box-sizing: border-box;
          touch-action: manipulation;
        }
      `}</style>
      </div>
    )
  }
}

export default Home;